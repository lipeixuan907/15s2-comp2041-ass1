#!/bin/bash
# 
# demo04.sh
# demo for nested for loop, echo -n, stdinput, input argument
# modified based on Ran Meng's demo

for n in n1 n2 n3 n4 n5
do
	for m in m1 m2 m3 m4 m5 m6 m7 m8m
	do
		for q in q10 q9 q8 q7 q6 q5 q4 q3 q2 q1 q0
		do
			echo $n $m $q
		done
	done
done

for n in contat the words together within one line without new line
do
	echo -n $n
done

# need three arguments
echo $1 $2 $3
read line
echo -n $line
exit 0