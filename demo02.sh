#!/bin/sh

# test for loop, nested if-else
# test *, test -r


for file in *.sh; do
	if test -r $file
	then
		echo $file is readable	# this file is readable
	else
		echo $file is not readable	# this file is not readable
	fi
done


# test echo -n, read
echo -n 'Please input your name:'	# test for single quotes
read name

# test for single quotes
echo 'Hello $name, this is a test for echo -n and read'
echo Sorry $name last line did not print your name because it use single quotes