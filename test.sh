#!/bin/bash

./shpy.pl $1 >pythonTest.py
sh $1 >sh.output
python -u pythonTest.py >py.output
diff py.output sh.output && echo "\n  Success!"
# rm py.output sh.output
echo '====================================================================================\n'
echo "  Python Code:"
echo '------------------------------------------------------------------------------------'
cat pythonTest.py
echo '####################################################################################'