#!/bin/sh

# print 2 power of x

# need one integer for argument
x=$1

start=0
result=1

while test $start -lt $x
do
    result=`expr $result \* 2`  # calculate
    start=`expr $start + 1`
done
echo $result

# test of pwd, ls, id, date
pwd
ls
id
date