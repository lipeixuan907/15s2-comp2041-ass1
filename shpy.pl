#!/usr/bin/perl -w
# COMP9041 Assignment 1 - shpy

use strict;

######################################
##			Initialization			##
######################################

our @lines = <>;
my $first_line = shift @lines;

# global variables
our @output_start = ();
our @output = ();
our $comment = 0;
# import flags with the position in the array of @output_start
our %import_flag = ();
# other flags
our $echo_n_flag = 0;
our $for_loop_flag = 0;
our $while_loop_flag = 0;
our $if_flag = 0;


if ($first_line =~ /^#!/) {
    push @output_start, "#!/usr/bin/python2.7 -u\n";
}


######################################
##			  Main Entry			##
######################################

while (scalar @lines > 0) {
    my $line = shift @lines;
    chomp $line;
    
    if ($line =~ /^# /) {  # comments
    	push @output, "$line\n";
    	next;
    } elsif ($line =~ /^(.*?\S*)(\s+#.*$)/) {  # comments followed by code
    	$comment = $2;
    	$line = $1;
    }

    if ($line =~ /^(\s*)$/) {  # print a new line
    	push @output, "$1\n";
    } elsif ($line =~ /^\s*echo/){  # translate echo to print
        &echo_to_print($line);
        if ($echo_n_flag != 0) {  # -n option
        	if (!exists $import_flag{import_sys}) {
        		push @output_start, "import sys\n";
        		$import_flag{import_sys} = $#output_start;
        	}
        	push @output, ",\nsys.stdout.write('')";
        } 
        if ($comment ne '0') {  # add comment if it exsit
        	push @output, $comment;
        	$comment = 0;
        }
        push @output, "\n";
        $echo_n_flag = 0;
    } elsif ($line =~ /(^\s*ls)|(^\s*pwd)|(^\s*id)|(^\s*date)/){  # import subprocess (ls pwd id date)
    	if (!exists $import_flag{import_subprocess}) {
    		push @output_start, "import subprocess\n";
    		$import_flag{import_subprocess} = $#output_start;
    	}
    	&import_subprocess($line);
    	if ($comment ne '0') {  # add comment if it exsit
        	push @output, $comment;
        	$comment = 0;
        }
    	push @output, "\n";
    } elsif ($line =~ /[a-zA-Z]+[0-9_]*[a-zA-Z]*=.+/){  # careate variable
    	&careate_variable($line);
    	if ($comment ne '0') {  # add comment if it exsit
        	push @output, $comment;
        	$comment = 0;
        }
    	push @output, "\n";
    } elsif ($line =~ /(^\s*cd )|(^\s*cd\s*$)/){  # import os cd
    	if (!exists $import_flag{import_os}) {
    		push @output_start, "import os\n";
    		$import_flag{import_os} = $#output_start;
    	}
    	&import_os($line);
    	if ($comment ne '0') {  # add comment if it exsit
        	push @output, $comment;
        	$comment = 0;
        }
    	push @output, "\n";
    } elsif (($line =~ /(^\s*for )/) || (($line =~ /(^\s*do)|(^\s*done)/) && ($for_loop_flag != 0))){	# for-loop
    	&for_loop($line);
    	if ($line =~ /(^\s*for )/) {
    		if ($comment ne '0') {  # add comment if it exsit
        		push @output, $comment;
        		$comment = 0;
        	}
    		push @output, "\n";
    	}
    }elsif (($line =~ /^\s*while /) || (($line =~ /(^\s*do)|(^\s*done)/) && ($while_loop_flag != 0))) {  # while-loop
    	&while_loop($line);
    	if ($line =~ /^\s*while /) {
    		if ($comment ne '0') {  # add comment if it exsit
        		push @output, $comment;
        		$comment = 0;
        	}
    		push @output, "\n";
    	}
    } elsif ($line =~ /(^\s*exit)|(^\s*read )/){  # import sys
    	if (!exists $import_flag{import_sys}) {
    		push @output_start, "import sys\n";
    		$import_flag{import_sys} = $#output_start;
    	}
    	&import_sys($line);
    	if ($comment ne '0') {  # add comment if it exsit
    	   	push @output, $comment;
    		$comment = 0;
    	}
    	push @output, "\n";
    } elsif (($line =~ /(^\s*if )/) || (($line =~ /(^\s*elif )|(^\s*else\s*)|(^\s*then\s*)|(^\s*fi)/) && ($if_flag != 0))) {  # if; elif; else
    	&if_elif_else($line);
    	if ($line =~ /(^\s*if )|(^\s*else)/) {
    		if ($comment ne '0') {  # add comment if it exsit
        		push @output, $comment;
        		$comment = 0;
        	}
    		push @output, "\n";
    	}
    }

    else { # Lines we can't translate are turned into comments
        push @output, "#$line\n";
    }
}

# print output
print @output_start;
print @output;

######################################
##				Level 0				##
######################################

# translate echo to print
sub echo_to_print($) {
	my $line = $_[0];
	my $text = "";
	if ($line =~ /(\s*)echo (.*)/) {
		push @output, $1;
		push @output, "print ";
		$text = $2;
		if ($text =~ /^(-n )(.*)/) {  # handle -n option <level 3>
			$echo_n_flag = 1;
			$text = $2;
		}
	} else {$text = $line}

	# single quotes	<level 2>
	if ($text =~ /^\'/) {
		$text =~ s/^(\'.*?\')(.*)/$2/;
		push @output, $1;
		if ($2 ne "") {  # process rest text
			push @output, ", ";
			&echo_to_print($text);
		}
		return;
	}

	# double quotes <level 3>
	if ($text =~ /^\"/) {
		$text =~ s/^(\".*?\")(.*)/$2/;
		push @output, $1;
		if ($2 ne "") {  # process rest text
			push @output, ", ";
			&echo_to_print($text);
		}
		return;
	}

	# quotes in the middle
	if ($text =~ /^(.*?)([\'\"].*)/) {
		&echo_to_print($1);
		&echo_to_print($2);
		return;
	}

	# do not have quotes	<level 0>
	my @words = split / /, $text;
	return if scalar @words == 0;
	if ($words[0] =~ /^[^\'\"]/) {
		while (scalar @words > 0){
			my $the_word = shift @words;
			# check for variables
			if ($the_word =~ /^\$/) {
				$the_word = &variable_translator($the_word);
				push @output, $the_word;
			}else {push @output, "\'" . $the_word . "\'";}

			if (scalar @words != 0) {
				push @output, ", ";
			}
		}
	} 
	
}

# import subprocess
sub import_subprocess($){
	my $line = $_[0];
	my @commands = split / /, $line;
	
	push @output, "subprocess.call([";
	while (scalar @commands > 0) {
		my $command = shift @commands;
		if ($command =~ /\$[\#\*\@]/) {  # variable
			$command = &variable_translator($command);
			push @output, " + $command";
			if (scalar @commands == 0) {
				push @output, ")" ;
				last;
			}
		} else {
			push @output, "\'" . $command . "\'";
		}

		if (scalar @commands == 0) {
			push @output, "])";
		} elsif ($commands[0] =~ /\$[\#\*\@]/) {
			push @output, "]";
		} else {
			push @output, ", ";
		}
	}
}

# careate variable
sub careate_variable($){
	my $line = $_[0];
	my @name_value = split /=/, $line;
	my $name = shift @name_value;
	my $value = shift @name_value;
	push @output, "$name = ";
	# a=$b
	if ($value =~ /^\$/) {
		$value = &variable_translator($value);
		push @output, "$value";
	} elsif ($value =~ /^\`.*\`$/) {
		&process_back_quotes($value);
	} else {push @output, "\'$value\'";}
}

# translate variable
sub variable_translator($) {
	my $line = $_[0];
	return $line if $line =~ /^[^\$]/;
	$line =~ s/\$//g;
	$line =~ s/\"//g;
	if ($line =~ /^([0-9\#\*\@])+$/) {	 # argv($1 $2 $3) <level 2>
		if (!exists $import_flag{import_sys}) {
    		push @output_start, "import sys\n";
    		$import_flag{import_sys} = $#output_start;
    	}
    	if ($1 eq '#') {  # $#
    		$line = "len(sys.argv[1:])";
    	} elsif ($1 eq '*') {  # $*
    		$line = "' '.join(sys.argv[1:])";
    	} elsif ($1 eq '@') {  # $@
    		$line = "sys.argv[1:]";
    	} else {  # $1 $2 $3
    		$line = "sys.argv[$line]";
    	}
	}
	return $line;
}


######################################
##				Level 1				##
######################################

# improt os
sub import_os($) {
	my $line = $_[0];
	my @commands = split / /, $line;
	my $command = shift @commands;

	if ($command =~ /^\s*cd$/){  # cd
		my $path = "";
		$path = shift @commands if scalar @commands > 0;
		if ($path =~ /^\s*~?$/) {
			push @output, "os.chdir(os.path.expanduser('~'))";
		} else {
			push @output, "os.chdir(\'$path\')";
		}
		
	}
}

# for-loop
sub for_loop($) {
	my $line = $_[0];
	my $space = "";
	for (my $i = 0; $i < $for_loop_flag + $while_loop_flag + $if_flag; $i++) {
		$space .= "	";
	}
	if ($line =~ /^(\s*)(for .*);\s*(do\s*)$/) {  # remove "do" if it in the end of the line
		unshift @lines, "$1$3";  # put do in a new line
		$line = "$1$2";
		$space = $1;
	}

	if ($line =~ /^\s*do\s*$/) {  # do things
		return;
	} elsif ($line =~ /^\s*done/){  # for-loop finish
		$for_loop_flag--;
		$for_loop_flag = 0 if $for_loop_flag < 0;
		return;
	}

	my @commands = split / /, $line;
	my $command = shift @commands;
	my $variable = shift @commands;
	my $in = shift @commands;
	if ($in =~ /^in$/) {
		push @output, "$space";
		push @output, "for $variable in ";
		while (scalar @commands > 0) {
			my $word = shift @commands;
			if ($word =~ /^[0-9]+$/) {  # for digits
				push @output, $word;
			} elsif ($word =~ /\*|\?|\[.+\]/){  # dealing glob
				if (!exists $import_flag{import_glob}) {
					push @output_start, "import glob\n";
					$import_flag{import_glob} = $#output_start;
				}
				push @output, "sorted(glob.glob(\"$word\"))";
			} else { # for others (string)
				push @output, "\'$word\'"
			}
			if (scalar @commands == 0){  # finish
				push @output, ":";
			} else {push @output, ", "}  # not finish
		}
		$for_loop_flag++;
	}
}

# import sys
sub import_sys($) {
	my $line = $_[0];
	if ($line =~ /(\s*)(exit) (.*)/){  # exit
		push @output, "$1" . "sys.$2($3)";
	} elsif ($line =~ /^(\s*)read (.*)/){  # read
		push @output, "$1$2 = sys.stdin.readline().rstrip()";
	}
}


######################################
##				Level 2				##
######################################

# if; elif; else
sub if_elif_else($) {
	my $line = $_[0];
	my $space = "";
	for (my $i = 0; $i < $for_loop_flag + $while_loop_flag + $if_flag; $i++) {
		$space .= "	";
	}

	if ($line =~ /^\s*then\s*?(.*)$/) {  # return for "then"		
		unshift @lines, "$1\n" if $1 ne "";
		return;
	}
	if ($line =~ /^\s*fi\s*$/) {  # return for "fi"
		$if_flag--;
		$if_flag = 0 if $if_flag < 0;
		return;
	}

	# "if" or "elif" line
	if ($line =~ /^\s*(if)|(elif) /) {
		if ($line =~ /^(\s*)([el]*?if .*);\s*(then\s*)$/) {  # remove "then" if it in the end of the line
			unshift @lines, "$space$3\n";  # put "then" in a new line
			$line = "$space$2";
		}
		if ($line =~ /^(\s*[el]*?if )(.*)$/) {
			push @output, $1;
			my $text = $2;
			if ($text =~ /^test /) {
				&process_test($text);
			}
			
			push @output, ":";
		}
	}
	# "else"
	if ($line =~ /^(\s*else)\s*?(.*)$/) {
		unshift @lines, "$2\n" if $1 ne "";
		push @output, "$space"."else:";
	}
	$if_flag++;
}

sub process_test($) {
	my $line = $_[0];

	# option
	if ($line =~ /^test -([rd]) (.+)$/) {  # option -r -d
    	if (!exists $import_flag{import_os}) {
    		push @output_start, "import os\n";
    		$import_flag{import_os} = $#output_start;
    	}

    	my $name = $2;
    	if ($1 eq "r") {  #-r
    		my $name_copy = $name;
    		$name = &variable_translator($name) if $name =~/^\$/;
    		$name = "\'$name\'" if $name_copy =~ /^[^\$]/;
    		push @output, "os.access($name, os.R_OK)";
    	} elsif ($1 eq "d") {  #-d
    		push @output, "os.path.isdir('$name')";
    	}
	}
	# compare string
	if ($line =~ /^test (\S+) (\!?=) (\S+)$/) {  # = !=
		my $v1 = $1;
		my $v3 = $3;
		my $string1 = &variable_translator($v1);
		my $sign = $2;
		my $string2 = &variable_translator($v3);
		$string1 = "\'$string1\'" if $v1 =~ /^[^\$]/;
		$string2 = "\'$string2\'" if $v3 =~ /^[^\$]/;
		if ($sign eq "!=") {  # !=
			push @output, "$string1 != $string2";
		} else {  # ==
			push @output, "$string1 == $string2";
		}
	}
	# compare integer
	if ($line =~ /^test \"?(\$?\S+)\"? -eq \"?(\$?\S+)\"?$/) {  # -eq ==
		my $variable1 = &variable_translator($1);
		my $variable2 = &variable_translator($2);
		push @output, "int($variable1) == int($variable2)";
	} elsif ($line =~ /^test \"?(\$?\S+)\"? -ge \"?(\$?\S+)\"?$/) {  # -ge >=
		my $variable1 = &variable_translator($1);
		my $variable2 = &variable_translator($2);
		push @output, "int($variable1) >= int($variable2)";
	} elsif ($line =~ /^test \"?(\$?\S+)\"? -gt \"?(\$?\S+)\"?$/) {  # -gt >
		my $variable1 = &variable_translator($1);
		my $variable2 = &variable_translator($2);
		push @output, "int($variable1) > int($variable2)";
	} elsif ($line =~ /^test \"?(\$?\S+)\"? -le \"?(\$?\S+)\"?$/) {  # -le <=
		my $variable1 = &variable_translator($1);
		my $variable2 = &variable_translator($2);
		push @output, "int($variable1) <= int($variable2)";
	} elsif ($line =~ /^test \"?(\$?\S+)\"? -lt \"?(\$?\S+)\"?$/) {  # -lt <
		my $variable1 = &variable_translator($1);
		my $variable2 = &variable_translator($2);
		push @output, "int($variable1) < int($variable2)";
	} elsif ($line =~ /^test \"?(\$?\S+)\"? -ne \"?(\$?\S+)\"?$/) {  # -ne !=
		my $variable1 = &variable_translator($1);
		my $variable2 = &variable_translator($2);
		push @output, "int($variable1) != int($variable2)";
	}
}


######################################
##				Level 3				##
######################################

sub while_loop($) {
	my $line = $_[0];
	my $space = "";
	for (my $i = 0; $i < $for_loop_flag + $while_loop_flag + $if_flag; $i++) {
		$space .= "	";
	}

	if ($line =~ /^(\s*)(while .*);\s*(do\s*)$/) {  # remove "do" if it in the end of the line
		unshift @lines, "$space$3";  # put "do" in a new line
		$line = "$space$2";
	}

	if ($line =~ /^\s*do/) {  # do things
		return;
	} elsif ($line =~ /^\s*done/){  # while-loop finish
		$for_loop_flag--;
		$for_loop_flag = 0 if $for_loop_flag < 0;
		return;
	}

	if ($line =~ /^(\s*while )(.*)$/) {
		push @output, $1;
		my $text = $2;
		if ($text =~ /^test /) {
			&process_test($text);
		}

		push @output, ":";
	}

	$while_loop_flag++;
}


sub process_back_quotes($) {
	my $line = $_[0];

	if ($line =~ /^\`\s*expr (.*)\`$/) {
		my @commands = split / /, $1;
		while (scalar @commands) {
			my $command = shift @commands;
			if ($command =~ /^\$/) {
				my $name = &variable_translator($command);
				push @output, "int($name)";
			} elsif ($command =~ /^\s*\\([\+\-\*\/])/) {
				push @output, $1;
			}
			else {
				push @output, $command;
			}
			push @output, " " if scalar @commands > 0;
		}
	}
}